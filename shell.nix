let
  pkgs = import (builtins.fetchTarball {
    name = "nixos-unstable-2024-11-09";
    url = "https://github.com/nixos/nixpkgs/archive/85f7e662eda4fa3a995556527c87b2524b691933.tar.gz";
    sha256 = "1p8qam6pixcin63wai3y55bcyfi1i8525s1hh17177cqchh1j117";
  }) { };
in
pkgs.mkShell {
  packages = [
    pkgs.asciidoctor
    pkgs.bashInteractive
    pkgs.cacert
    pkgs.check-jsonschema
    pkgs.deadnix
    pkgs.gitFull
    pkgs.gitlint
    pkgs.nixfmt-rfc-style
    pkgs.nodePackages.prettier
    pkgs.pre-commit
    pkgs.python3Packages.jsonschema
    pkgs.shellcheck
    pkgs.shfmt
    pkgs.statix
    pkgs.yq-go
  ];
  env = {
    LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  };
}
